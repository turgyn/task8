package com.example.demo.auth;

import com.example.demo.app.service.UserService;
import com.example.demo.auth.dto.AuthCreds;
import com.example.demo.auth.jwt.JwtProvider;
import com.example.demo.app.entity.User;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@Log
public class AuthController {

    private final UserService userService;

    private final JwtProvider jwtProvider;

    public AuthController(UserService userService, JwtProvider jwtProvider) {
        this.userService = userService;
        this.jwtProvider = jwtProvider;
    }

    @PostMapping("/register")
    public ResponseEntity registerUser(@RequestBody @Valid AuthCreds request) {
        User user = new User(
                request.getUsername(),
                request.getPassword()
        );
        if (userService.create(user)) {
            return ResponseEntity.ok("User created");
        }
        return ResponseEntity.badRequest().body("username already exists");
    }

    @PostMapping("/auth")
    public ResponseEntity auth(@RequestBody @Valid AuthCreds request) {
        User user = userService.findByLoginAndPassword(request.getUsername(), request.getPassword());
        if (user != null) {
            String token = jwtProvider.generateToken(user.getUsername());
            HashMap<String, String> response = new HashMap<>();
            response.put("token", token);
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.badRequest().body("Wrong username or password");
    }
}
