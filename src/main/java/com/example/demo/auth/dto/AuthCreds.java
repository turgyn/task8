package com.example.demo.auth.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class AuthCreds {
    @NonNull
    private String username;
    @NonNull
    private String password;
}
