package com.example.demo.auth.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class AuthResponse {
    @NonNull
    private String token;
}
