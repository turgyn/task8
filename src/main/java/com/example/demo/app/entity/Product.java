package com.example.demo.app.entity;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "products")
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    private Long id;

    @NotBlank
    private String name;

    @Min(value = 0, message = "Not negative value")
    private int price;

    @Min(value = 0, message = "Not negative value")
    private int amount;

    public Product() {}
}
