package com.example.demo.app.entity;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 4, max = 20, message = "length 4-20 expected")
    @NonNull
    private String username;

    @NotBlank
    @NonNull
    private String password;

    @Min(value = 0, message = "Positive value expected")
    private Long balance = 0L;

    private String role = "ROLE_USER";

    public User() {

    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
