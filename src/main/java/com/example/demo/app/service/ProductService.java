package com.example.demo.app.service;

import com.example.demo.app.entity.Product;
import com.example.demo.app.entity.User;
import com.example.demo.app.exceptions.NotEnoughMoneyException;
import com.example.demo.app.exceptions.NotEnoughProductException;
import com.example.demo.app.repository.ProductRepository;
import com.example.demo.app.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    public ProductService(ProductRepository productRepository, UserRepository userRepository) {
        this.productRepository = productRepository;
        this.userRepository = userRepository;
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public void create(Product product) {
        productRepository.save(product);
    }

    public void purchase(Long prodId, String username) throws NotEnoughMoneyException, NotEnoughProductException{
        Product product = productRepository.getOne(prodId);
        User user = userRepository.findByUsername(username);

        int prodPrice = product.getPrice();
        int prodAmount = product.getAmount();
        Long userBalance = user.getBalance();

        if (userBalance < prodPrice) {
            throw new NotEnoughMoneyException(userBalance, prodPrice);
        }
        if (prodAmount == 0) {
            throw new NotEnoughProductException();
        }

        if (userBalance >= prodPrice && prodAmount > 0) {
            product.setAmount(prodAmount-1);
            user.setBalance(userBalance - prodPrice);
            productRepository.save(product);
            userRepository.save(user);
        }
    }
}
