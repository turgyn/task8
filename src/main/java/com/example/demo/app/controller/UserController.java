package com.example.demo.app.controller;

import com.example.demo.app.entity.User;
import com.example.demo.app.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getUsers() {
        return userService.findUsers();
    }

    @GetMapping("/{username}")
    public String getUser(@PathVariable String username) {
        return username;
    }
}
