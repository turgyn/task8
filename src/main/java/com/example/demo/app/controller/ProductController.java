package com.example.demo.app.controller;

import com.example.demo.app.entity.Product;
import com.example.demo.app.exceptions.NotEnoughMoneyException;
import com.example.demo.app.exceptions.NotEnoughProductException;
import com.example.demo.app.service.ProductService;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/products")
@Log
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<Product> getProducts() {
        return productService.findAll();
    }

    @PostMapping("/new")
    public ResponseEntity newProduct(@RequestBody @Valid Product product) {
        log.info("cont started");
        productService.create(product);
        return ResponseEntity.ok("New Product created");
    }

    @PostMapping("/{id}/buy")
    public ResponseEntity purchase(@PathVariable Long id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
        try {
            if (principal instanceof UserDetails) {
                UserDetails user = (UserDetails) principal;
                productService.purchase(id, user.getUsername());
                return ResponseEntity.ok("purchase completed");
            }
        } catch (NotEnoughMoneyException e) {
            return ResponseEntity.badRequest().body("Not enough money, " + e.toString());
        } catch (NotEnoughProductException e) {
            return ResponseEntity.badRequest().body("Not Enough products");
        }
        return ResponseEntity.badRequest().body("Some error occurs");
    }
}
