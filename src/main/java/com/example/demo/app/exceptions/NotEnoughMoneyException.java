package com.example.demo.app.exceptions;

public class NotEnoughMoneyException extends Exception {
    public NotEnoughMoneyException(Long balance, int price) {
        super("Balance: " + balance + ", Product price: " + price);
    }
}
